﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Forms;

namespace MayaTumbler
{
    public partial class Form1 : Form
    {
        System.Timers.Timer tim = new System.Timers.Timer(1000);
        string nameWindow = "";

        public Form1()
        {
            InitializeComponent();

            NotifyIcon icon = new NotifyIcon();
            icon.Icon = Properties.Resources.icon;
            icon.Visible = true;

            icon.MouseDown += Icon_MouseDown;
            
            ShowWindow(GetConsoleWindow(), 0);
            tim.Elapsed += Tim_Elapsed;
            tim.Start();

            
        }
        private void Icon_MouseDown(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(Cursor.Position);
            }
           
        }

        private void Tim_Elapsed(object sender, ElapsedEventArgs e)
        {
            IntPtr h = GetForegroundWindow();
            int pid = 0;
            GetWindowThreadProcessId(h, ref pid);
            Process p = Process.GetProcessById(pid);
            nameWindow = p.MainWindowTitle;
            Console.WriteLine("pid: {0}; window: {1}", pid, p.MainWindowTitle);

            if (p.MainWindowTitle.Split(' ')[0] == "Autodesk")
            {
                string lang = "00000409";
                int ret = LoadKeyboardLayout(lang, 1);
                PostMessage(GetForegroundWindow(), 0x50, 1, ret);
            }

        }



        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }






        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        public static extern UInt32 GetWindowThreadProcessId(IntPtr hwnd, ref Int32 pid);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool PostMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        static extern int LoadKeyboardLayout(string pwszKLID, uint Flags);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        
    }
}
